1.0.1.1 (2021-03-24)
-------------------

- Added support for location (EU)

1.0.1 (2020-07-05)
-------------------

- Fixed an issue when the source tap can include uppercase primary keys

1.0.0 (2020-06-09)
-------------------

- Initial release
